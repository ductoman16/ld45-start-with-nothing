extends KinematicBody2D
class_name Player

export var jump_speed = 300
export var jump_acceleration = .3

export var gravity = 10
export var terminal_velocity = 500

export var horizontal_speed = 220
export var horizontal_acceleration = .25
export var friction = .2

export(NodePath) var UI
var ui: CanvasLayer

onready var animator = $AnimationPlayer

var velocity = Vector2()
var is_jumping = false

func _ready() -> void:
	print("Player ready")
	animator.play("idle")
	ui = get_node(UI)
	#load_unlocked_powerups()


func _physics_process(delta):
	velocity.y = min(velocity.y + gravity, terminal_velocity)
	var snap = Vector2.DOWN * 32 if is_jumping else Vector2.ZERO
	move_and_slide_with_snap(velocity, snap, Vector2.UP)


func load_unlocked_powerups():
	for powerup_script in UnlockedPowerups.unlocked_powerups:
		load_powerup(powerup_script)


func load_powerup(powerup_script):
	print("adding powerup: " + powerup_script)
	var powerup_class = load(powerup_script)
	var powerup = powerup_class.new()
	add_child(powerup)
	return powerup


func add_new_powerup(powerup_script):
	var powerup = load_powerup(powerup_script)
	#UnlockedPowerups.unlocked_powerups.append(powerup_script)
	ui.show_toast(powerup)
