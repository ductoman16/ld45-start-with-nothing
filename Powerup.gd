extends Area2D
class_name Powerup


onready var Player =  $Player

export(String, FILE, "*.gd") var powerup_script = ""


func _ready() -> void:
	self.connect("body_entered", self, "_on_Powerup_body_entered")


func _on_Powerup_body_entered(body: PhysicsBody2D) -> void:
	if !is_instance_valid(body):
		return

	print(body.name + " Entered Powerup with unlock " + powerup_script)

	var player = body as Player
	if player != null:
		player.add_new_powerup(powerup_script)

		queue_free()
