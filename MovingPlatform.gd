extends Node2D

const idle_duration = 1
const tile_size = 16

export var move_to = Vector2.RIGHT * 64
export var speed = 3

onready var platform = $Platform
onready var tween = $Tween

var follow = Vector2.ZERO


func _ready() -> void:
	_init_tween()


func _init_tween():
	var duration = move_to.length() / float(speed * tile_size)
	print(duration)
	tween.interpolate_property(self, "follow", Vector2.ZERO, move_to, duration, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT, idle_duration)
	tween.interpolate_property(self, "follow", move_to, Vector2.ZERO, duration, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT, duration + (idle_duration * 2))
	tween.start()


func _physics_process(delta: float) -> void:
#	print(follow)
	platform.position = platform.position.linear_interpolate(follow, .08)


func _on_Tween_tween_started(object: Object, key: NodePath) -> void:
#	print("Tween started: " + key)
	pass


func _on_Tween_tween_completed(object: Object, key: NodePath) -> void:
#	print("Tween finished: " + key)
	pass