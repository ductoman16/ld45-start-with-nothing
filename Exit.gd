extends AnimatedSprite

export(String, FILE) var next_level


func _on_Area2D_body_entered(body: PhysicsBody2D) -> void:
	if !is_instance_valid(body):
		return

	print("Exit triggered")

	var player = body as Player
	if player != null:
		get_tree().change_scene(next_level)
