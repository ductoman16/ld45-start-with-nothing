extends Node2D

onready var player = $Player

func _ready() -> void:
	player.load_powerup("res://Powerups/MoveRight.gd")
	player.load_powerup("res://Powerups/MoveLeft.gd")
	player.load_powerup("res://Powerups/Friction.gd")