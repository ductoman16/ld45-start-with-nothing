extends Node2D

const fall_time = 10

export(String, FILE) var next_level

onready var animator = $AnimationPlayer

var timer: Timer = Timer.new()
var starting = false

func _ready() -> void:
	timer.set_one_shot(true)
	timer.set_wait_time(fall_time)
	timer.connect("timeout", self, "on_timeout_complete")
	add_child(timer)


func _input(event):
	if event is InputEventKey and event.pressed:
		if !starting:
			start_game()


func start_game():
	starting = true
	animator.play("fade_out")
	timer.start()


func on_timeout_complete():
	get_tree().change_scene(next_level)