extends Area2D

const death_time = .3

onready var death_message = $CanvasLayer/DeathMessage

var timer: Timer = Timer.new()

func _ready() -> void:
	pause_mode = PAUSE_MODE_PROCESS
	timer.set_one_shot(true)
	timer.set_wait_time(death_time)
	timer.connect("timeout", self, "on_timeout_complete")
	add_child(timer)


func _on_body_entered(body: PhysicsBody2D) -> void:
	if !is_instance_valid(body):
		return

	print(body.name + " collided with " + self.name)
	var player = body as Player
	if player != null:
		do_player_death()


func do_player_death():
	print("do_player_death")
	get_tree().paused = true
	death_message.show()
	timer.start()


func on_timeout_complete():
	print("reloading scene")
	get_tree().paused = false
	get_tree().reload_current_scene()
