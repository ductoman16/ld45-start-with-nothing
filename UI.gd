extends CanvasLayer
class_name UI

export var time_to_show = 7

onready var unlock_toast: Label = $UnlockToast
onready var animator = $AnimationPlayer

var timer: Timer = Timer.new()


func _ready() -> void:
	timer.set_one_shot(true)
	timer.set_wait_time(time_to_show)
	timer.connect("timeout", self, "on_timeout_complete")
	add_child(timer)


func show_toast(powerup):
	unlock_toast.set_text(powerup.description)
	unlock_toast.show()
	timer.start()


func on_timeout_complete():
	animator.play("fade_out")

