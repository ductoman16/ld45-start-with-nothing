extends Node
class_name MoveLeft

const display_name = "Walk Left"
const description = "Unlocked: Move Left. You can now move the other direction!"

onready var parent = get_parent()

func _physics_process(delta: float) -> void:
	if Input.is_action_pressed("move_left"):
		parent.velocity.x = lerp(parent.velocity.x, -parent.horizontal_speed, parent.horizontal_acceleration)
		parent.animator.play("walk_left")