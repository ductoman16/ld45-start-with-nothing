extends Node
class_name Friction

const display_name = "Friction"
const description = "Powerup unlocked: Friction! You can now stand still!"

onready var parent = get_parent()


func _physics_process(delta: float) -> void:
	if !Input.is_action_pressed("move_left") && !Input.is_action_pressed("move_right"):
		parent.velocity.x = lerp(parent.velocity.x, 0, parent.friction)
		parent.animator.play("idle")