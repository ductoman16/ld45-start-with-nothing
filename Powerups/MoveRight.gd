extends Node
class_name MoveRight

const display_name = "Walk Right"
const description = "What's this? You fell onto a power-up! Looks like you can move to the right now. And nothing more."

onready var parent = get_parent()

func _physics_process(delta: float) -> void:
	if Input.is_action_pressed("move_right"):
		parent.velocity.x = lerp(parent.velocity.x, parent.horizontal_speed, parent.horizontal_acceleration)
		parent.animator.play("walk_right")
