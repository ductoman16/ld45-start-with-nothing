extends Node
class_name Jump

const display_name = "Jump"
const description = "You can finally jump! Now go and jump!"

onready var parent = get_parent()


func _physics_process(delta: float) -> void:
	if parent.is_on_floor():
		parent.is_jumping = false
		if Input.is_action_just_pressed("jump"):
			parent.is_jumping = true
			parent.velocity.y = -parent.jump_speed